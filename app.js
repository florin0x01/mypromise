var express = require('express');
var path = require('path');
var fs = require('fs');
var crypto = require('crypto');

//var api = null;

var app = express();

// Define the port to run on
app.set('port', 3000);
app.use(express.static(path.join(__dirname, 'public')));

// Listen for requests
var server = app.listen(app.get('port'), function() {
  var port = server.address().port;
  console.log('Magic happens on port ' + port);
});

app.post('/profile', function(req, res) {
    console.log(req);  

    if ( ('binStuff' in req) === false ) {
        console.log("filechunk not in request");
        return 'Error';
    }
    else
      console.log("Filechunk in request");

    let body = Buffer.from(req.body, 'binary');
    console.log("Body: ", body);
    var hash = crypto.createHash('md5').update(body).digest('hex');
    console.log("HASH: ", hash);  
    fs.writeFile('/tmp/fis1', body, function(err) {
        if(err) {
          console.log("Error");
          return console.log(err);
        } 
        console.log("The file was saved!");
        res.send('POST request to the homepage') 
    });
});
