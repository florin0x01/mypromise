function AfterX(num) {
    var x = new Date();
    x.setDate(x.getDate()+num);
    return x;
}
// a and b are javascript Date objects
function dateDiffInDays(a, b) {
  // Discard the time and time-zone information.
  var _MS_PER_DAY = 1000 * 60 * 60 * 24;
  
  var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
  var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

  return Math.floor((utc2 - utc1) / _MS_PER_DAY);
}

let passStates = {
    step1: 0,
    step2: 0,
    step3: 0,
    step4: [0,0],
    step5: 0,
    step6: 0
}

let bState = {
    promise: '',
    deadline: '',
    img: '',
    name: '',
    mails: {
        mine: '',
        friend: ''
    }
}

function step1() {
    $("#step1").css("visibility", "visible");
    $("#step2").hide();
    $("h1").html("My promise");
    $("#step1").fadeIn(500);
    typed.start();
}

function step2() {
    $("#step2").css("visibility", "visible");
    $("#step1").hide();  
    $("#deadline").val("");
    $("h1").html("The deadline");
    $("#step2").fadeIn(500);
    $("#step2next").prop('disabled', true);
}

function step3() {
    $("#step3").css("visibility", "visible");
    $("#step2").hide();  
    $("h1").html("Your pic");
    $("#step3").fadeIn(500);
    $("#step3next").prop('disabled', true);
    startup();
}

function step4() {
    $("#step4").css("visibility", "visible");
    $("#step3").hide();
    $("h1").html("Your info");
    $("#step4").fadeIn(500);
    $("#step4next").prop('disabled', true);
}

function step5() {
    $("#step5").css("visibility", "visible");
    $("#step4").hide();
    $("h1").html("Last step");
    $("#step5next").prop('disabled', true);
}

function step6(msg="Thanks") {
    $("#step5").hide();    
    $("h1").html(msg);
    xconsolelog(bState);
}

function uiEmailValidation(elem) {
    if (elem.val().length > 2) {
        if (!validateEmail(elem.val())) {
            elem.css("color", "#ee0000");
            return false;
        }       
        else { 
            elem.css("color", "#000000"); 
            return true;
        }
    }
}

function install() {

    $("#step2").hide();
    $("#step2prev").on('click', () => {
        $("#step2").fadeOut(500);
        step1();        
    });

    $('#mypromis').on('click', () => {
        typed.stop();
    });

    $("#step3prev").on('click', () => {
        let nextState = $("#step3prev").attr('nextState');
        if (trackingTask != null) { 
            trackingTask.stop();
            trackingTask = null;
        }
        if (intervalHandle != null) {
            clearInterval(intervalHandle);
            intervalHandle = null;
        }
        $("#step3").fadeOut(500);
        $("#photo").css('display', 'none');    
        
        if (myStream != null) {
            myStream.getTracks()[0].stop();
            myStream = null;                
        }       
        if (nextState == "goback") {  
            step2();
        }
        else if (nextState == "cam") step3();
        else step2(); //default to step2     
    });

    $("#step1next").on('click', () => {
        $("#step1").fadeOut(500);
        if (wInterval != null) {
            clearInterval(wInterval);
            wInterval = null;
        }
        typed.stop();
        bState.promise = $("#mypromis").val() === "" ? $("#mypromis").attr('placeholder') : $("#mypromis").val();
        step2();
    });

    $("#step2next").on('click', () => {
        $("#step2").fadeOut(500);
        bState.deadline = $("[name='deadline']").val();
        step3();
    });

    $("#step3next").on('click', () => {
        $("#step3").fadeOut(500);
        step4();
    });

    $("#step4next").on('click', () => {
        bState.name = $("#name").val();
        bState.mails.mine = $("#myemail").val().toLowerCase();
        $("#step4").fadeOut(500);
        step5();
    });

    $("#step5next").on('click', () => {
        bState.mails.friend = $("#email").val().toLowerCase();
        grecaptcha.execute();
    });

    $("#startbutton").on('click', (ev) => {
        takepicture();
        ev.preventDefault();
    });

    $("#mypromis").on('click', (ev) => {
        $("#mypromis").val("");
        $("#mypromis").attr('placeholder', 'Enter a goal'); 
        passStates['step1'] = $("#mypromis").val().length > 3 ? 1 : 0;
        doStateBut($("#step1next"), passStates['step1']);   
    });
    
    $("#mypromis").on('input', (ev) => {
        
        passStates['step1'] = $("#mypromis").val().length > 3 ? 1 : 0;
        doStateBut($("#step1next"), passStates['step1']);    
    });

    $("#name").on('input', (ev) => {
        if ($("#name").val().length < 2) { 
            $("#name").css("color", "#ee0000");
            passStates['step4'][0] = 0;
        }  
        else {
            $("#name").css("color", "#000000");
            passStates['step4'][0] = 1;
        }    
        doStateBut($("#step4next"),(passStates['step4'][0]+passStates['step4'][1])/passStates['step4'].length);
    });

    $("#myemail").on('input', (ev) => {
        if (!uiEmailValidation($("#myemail")))
            passStates['step4'][1] = 0;
        else
            passStates['step4'][1] = 1; 
        doStateBut($("#step4next"),(passStates['step4'][0]+passStates['step4'][1])/passStates['step4'].length);
    });

    function step5Validate() {
        if (!uiEmailValidation($("#email")) || $("#aok").prop('checked') === false)
            passStates['step5'] = 0;
        else if ($("#email").val().toLowerCase() === $("#myemail").val().toLowerCase()) {
            $("h1").html("Please enter a friend's email..");
            passStates['step5'] = 0;
        }
        else {
            $("h1").html("Last step");
            passStates['step5'] = 1;
        }
        doStateBut($("#step5next"), passStates['step5']);
    }

    $("#email").on('input', (ev) => {
        step5Validate();
    });

    $("#aok").on('click', (ev) => {
        step5Validate();
    });

    $("#deadline").pickadate({
        today: '',
        clear: '',
        close: 'Cancel',
        formatSubmit: 'yyyy/mm/dd',
        hiddenName: true,
        min: AfterX(3), //TODO set for at leats 2 days
        max: AfterX(90),
        onSet: function(context) {
            $("#step2next").prop('disabled', false);
        }
    });
}