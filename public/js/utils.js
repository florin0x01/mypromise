function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function doStateBut(elm, checksOk) {
    if (checksOk === 1 || checksOk >= 1)  
        elm.prop('disabled', false);
    else 
        elm.prop('disabled', true);
}

function resetStates() {
    passStates = {
        step1: 0,
        step2: 0,
        step3: 0,
        step4: [0,0],
        step5: 0,
        step6: 0
    }
}

function xconsolelog(args) {
    let _debug = false;
    if (_debug === true)
        console.log(args);
}

function formatDate(date) {
    var d = null;
    if (date === null || date === undefined) d = new Date();
    else d = new Date(date);

    var month = '' + (d.getMonth() + 1), day = '' + d.getDate(), year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}