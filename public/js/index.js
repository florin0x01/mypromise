var defaults = [
    "Give up smoking",  
    "Cook more often",
    "Go to the gym more often",
    "Travel more often",
    "Study for my exam",
    "Learn a new language",
    "Make more friends",
    "Quit my job",
    "Go to bed earlier",
    "Work on my startup",
    "Finish my book on time",
    "Graduate on time",
    "Quit drinking",
    "Stop fooling around",
    "Get a job"
]

function cycle() {
    let idx = Math.floor(Math.random() * defaults.length);
    xconsolelog("IDx ", idx);
    return defaults[idx];
}

function shuffle (array) {
    var i = 0
      , j = 0
      , temp = null
  
    for (i = array.length - 1; i > 0; i -= 1) {
      j = Math.floor(Math.random() * (i + 1))
      temp = array[i]
      array[i] = array[j]
      array[j] = temp
    }

    return array;
  }
  