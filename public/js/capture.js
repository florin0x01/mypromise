
    // The width and height of the captured photo. We will set the
    // width to the value defined here, but the height will be
    // calculated based on the aspect ratio of the input stream.
  
    var width = screen.width < 640 ? 320 : 640;    // We will scale the photo width to this
    //var width = 640;
    var height = 0;     // This will be computed based on the input stream
  
    // |streaming| indicates whether or not we're currently streaming
    // video from the camera. Obviously, we start at false.
  
    var streaming = false;
  
    // The various HTML elements we need to configure or contro l. These
    // will be set by the startup() function.
  
    var video = null;
    var canvas = null;
    var photo = null;
    var startbutton = null;
    var myStream = null;
    var trackingTask = null;
    var intervalHandle = null;
    var picTime = 12000;

    // https://stackoverflow.com/questions/9281118/how-do-i-display-play-forward-or-solid-right-arrow-symbol-in-html

    function startup() {
      myStream = null; streaming = false; canvas = null; video=null;
      video = document.getElementById('video');
      canvas = document.getElementById('canvas');
      photo = document.getElementById('photo');

      //video.setAttribute('style', 'display: initial');
      positionVideo();

      startbutton = document.getElementById('startbutton');
      $("#step3next").prop('disabled', true);
      $("#step3next").css("display", "none");
            
      $("#step3prev").html("Back  &#9668;");
      $("#step3prev").attr("nextState", "goback");  
      $("h1").html("Say cheese..")    
      let elapsedT = picTime;

     // trackVideo();  
            
      intervalHandle = setInterval(function(){  
          if (elapsedT === 0) {
             takepicture();
             $("h1").html("Your pic");
             $("#step3next").prop('disabled', false);
             $("#step3next").css("display", "initial");
             clearInterval(intervalHandle);
             intervalHandle = null;
          }
          else
            $("h1").html(elapsedT / 1000);
          elapsedT = elapsedT - 1000;
          xconsolelog(elapsedT);

      }, 1000);

      navigator.getMedia = ( navigator.getUserMedia ||
                             navigator.webkitGetUserMedia ||
                             navigator.mozGetUserMedia ||
                             navigator.msGetUserMedia );
  
      navigator.getMedia(
        {
          video: true,
          audio: false
        },
        function(stream) {
          xconsolelog("My stream: ", stream);
          myStream = stream;
        
          if (navigator.mozGetUserMedia) {
            video.mozSrcObject = stream;
            xconsolelog("Got src video")
          } else {
            var vendorURL = window.URL || window.webkitURL;
            video.src = vendorURL.createObjectURL(stream);
            xconsolelog("Got src video");
          }
          xconsolelog("Playing video");
          video.play();
        },
        function(err) {
            xconsolelog("An error occured! " + err);
            $("h1").html("There's no webcam...");
            clearInterval(intervalHandle);
            $("#startbutton").prop('disabled', true);
        }
      );
  
      video.addEventListener('canplay', function(ev){
        
        if (!streaming) {
          height = video.videoHeight / (video.videoWidth/width);
        
          // Firefox currently has a bug where the height can't be read from
          // the video, so we will make assumptions if this happens.
        
          if (isNaN(height)) {
            height = width / (4/3);
          }

          console.log("Width, height: ", width, height);
        
          video.setAttribute('width', width);
          video.setAttribute('height', height);
          canvas.setAttribute('width', width);
          canvas.setAttribute('height', height);
          streaming = true;
        }
      }, false);
 
        clearphoto();
    }
  
    // Fill the photo with an indication that none has been
    // captured.
  
    function clearphoto() {
        var context = canvas.getContext('2d');
        context.fillStyle = "#AAA";
        context.fillRect(0, 0, canvas.width, canvas.height);
        var data = canvas.toDataURL('image/png');
        photo.setAttribute('src', data);
    }

    function dataURItoBlob(canvas,quality,type) {
      var dataURL = canvas.toDataURL(type, quality);

      var binStr = atob( dataURL.split(',')[1] ),
      len = binStr.length,
      arr = new Uint8Array(len);

      for (var i = 0; i < len; i++ ) {
          arr[i] = binStr.charCodeAt(i);
      }

      return new Blob( [arr.buffer], {type: type || 'image/png'} );
    }

    function positionPhoto() {
      $("#photo").css('position', 'relative');
      $("#photo").css('display', 'block');
      return;
    }

    function positionVideo() {
      $("#video").css('position', 'relative');
      $("#video").css('display', 'block');
      return;
    }

  
    // Capture a photo by fetching the current contents of the video
    // and drawing it into a canvas, then converting that to a PNG
    // format data URL. By drawing it on an offscreen canvas and then
    // drawing that to the screen, we can change its size and/or apply
    // other changes before drawing it.
    function takepicture() {
        xconsolelog("Takepicture ");
        var context = canvas.getContext('2d');
        video.setAttribute('style', 'display: none');
        if (width && height) {
            canvas.width = width;
            canvas.height = height;
            context.drawImage(video, 0, 0, width, height);
            
            canvas.toBlob((blob)=> {
                photo.setAttribute('src', URL.createObjectURL(blob));
                bState.img = blob;
                stopPics();
                positionPhoto();
            }, 'image/jpeg', 0.7);

          } else {
            xconsolelog("Clearing photo, no h/w")
            clearphoto();
        }
    }

    function stopPics() {
        myStream.getTracks()[0].stop();
        streaming = false;        
        $("#step3prev").html("New pic <span class='glyphicon glyphicon-camera'></span>")
        $("#step3prev").attr("nextState", "cam");  
    }
  
    function trackVideo() {
        var tracker = new tracking.ObjectTracker('face');
        var trackingCanvas = document.getElementById('tracking_can');
        var trackingCanvasCtx = trackingCanvas.getContext('2d');

        
        tracker.setInitialScale(1.5);
        tracker.setStepSize(1);
        tracker.setEdgesDensity(0.1);

        
/*
         tracker.setInitialScale(4);
         tracker.setStepSize(2);
         tracker.setEdgesDensity(0.1);
  */

  
        inProcessOfStop = false;

        trackingTask = tracking.track('#video', tracker);          

        tracker.on('track', function(event) {

            if (trackingCanvas.width !=0)
              trackingCanvasCtx.clearRect(0, 0, trackingCanvas.width, trackingCanvas.height);
            if (event.data.length > 0) {
          //   xconsolelog("Got event: ", event.data.length);
            }
            event.data.forEach(function(rect) {
              trackingCanvasCtx.strokeStyle = '#a64ceb';
              trackingCanvasCtx.strokeRect(rect.x, rect.y, rect.width, rect.height);
              trackingCanvasCtx.font = '11px Helvetica';
              trackingCanvasCtx.fillStyle = "#fff";
            // trackingCanvasCtx.fillText('x: ' + rect.x + 'px', rect.x + rect.width + 5, rect.y + 11);
              //trackingCanvasCtx.fillText('y: ' + rect.y + 'px', rect.x + rect.width + 5, rect.y + 22);
              
                if (intervalHandle == null) {
                    if (trackingTask != null) {
                        trackingTask.stop();
                        trackingTask = null;
                        takepicture();
                        $("h1").html("Your pic");
                        $("#step3next").prop('disabled', false);
                        $("#step3next").css("display", "initial");
                        tracking_can.setAttribute('style', 'display: none');     
                    }
                  
                }     
            });
        });
  }
