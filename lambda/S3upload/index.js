const querystring = require('querystring');
const striptags = require('striptags');
const AWS = require('aws-sdk')
const https = require('https')
const util = require('util');

const formatSeparator = '#';
 
//PUTS objects into S3 bucket of promises

//S3 listObjectVersions, listObjectsV2
//PUT Notification S3 -> send SQS email (?????) 
//OR classic way -> API GW -> Lambda -> S3 upload + send email for confirmation

//Expiration S3 notification -> Lambda get metadata obj (bState ??) + generate another email (Link Yes/NO success promise)
//if yes, -> send certificate with pic
//if no, -> ??

// a and b are javascript Date objects
function dateDiffInDays(a, b) {
    // Discard the time and time-zone information.
    var _MS_PER_DAY = 1000 * 60 * 60 * 24;
    
    var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
    var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());
  
    return Math.floor((utc2 - utc1) / _MS_PER_DAY);
}

function formatDate(date) {
    var d = null;
    if (date === null || date === undefined) d = new Date();
    else d = new Date(date);

    var month = '' + (d.getMonth() + 1), day = '' + d.getDate(), year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}

exports.handler = (request, context, callback) => {
    // TODO implement
    //console.log("API Event", event);

    let responseCode = 200;
    let responseBody = { status: 'OK' };

    console.log("Headers: ", request);

    var response = {
        statusCode: responseCode,
        headers: {
            "access-control-allow-origin": "*"
        },
        body: JSON.stringify(responseBody)
    };

    let promise     = request.headers['capp-promise'];
    let deadline    = request.headers['capp-deadline'];
    let name        = request.headers['capp-name'];
    let mail        = request.headers['capp-mail'];
    let friend      = request.headers['capp-friend'];

    if (!promise || !deadline || !name || !mail || !friend)
        return callback(null, {
            statusCode: 401,
            body: 'Invalid params'
        });

    var deadlineDays = dateDiffInDays(new Date(), new Date(deadline));

    console.log("Deadline days: ", deadlineDays);

    //AWS.region = 'us-east-2';

    if (request.body !== null && request.body !== undefined) {
        //let img = JSON.parse(request.body);
        var s3 = new AWS.S3();
        var ts = Math.round((new Date()).getTime() / 1000);
    
        const buf = Buffer.from(request.headers['capp-promise'], 'utf-8');

        var params = {
            Body: request.body, 
            Bucket: "mypromise-storage0", 
            Key: "ex" + deadlineDays + "/" + name + formatSeparator + mail + formatSeparator + friend 
                    + formatSeparator + buf.toString('base64') 
                    + "/" + deadline + "/" + request.requestContext.identity.sourceIp
                    + "/photo-" + ts + ".jpg", 
         //   ServerSideEncryption: "AES256", 
            Metadata: {
                "promise":   request.headers['capp-promise'],
                "start":     formatDate(),   
                "deadline":  request.headers['capp-deadline'],
                "daysRem":   deadlineDays.toString(),
                "name":      request.headers['capp-name'],
                "mail":      request.headers['capp-mail'],
                "friend":    request.headers['capp-friend'],
                "ip":        request.requestContext.identity.sourceIp,
            }
        };  
        s3.putObject(params, function(err,data){
            if (err) { 
                console.log(err, err.stack);
                return callback('Error', {
                    statusCode: 400,
                    body: JSON.stringify({status:'Server upload error'})
                });
            } 
            else  {
                console.log(data);           
                // successful response
                //send email      
                return callback(null, response);                 
            } 
        });      
     }

    return callback(null, response); 
};


