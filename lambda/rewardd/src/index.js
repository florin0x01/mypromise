const pdf = require('pdfjs/lib');
const fs = require('fs');
var crypto = require('crypto');
var base64 = require('file-base64');

// https://github.com/rkusa/pdfjs

var pdfFileName = '';
const EVT_SOURCE_ERR = "[MYPROMISE_LAMBDA_REWARD][ERR] ";
const EVT_SOURCE_OK = "[MYPROMISE_LAMBDA_REWARD][OK] ";

let _sendEmail = true;

const fontData = {
    'HelveticaBold': {"fontName":"Helvetica-Bold","fullName":"Helvetica Bold","familyName":"Helvetica","italicAngle":0,"characterSet":"ExtendedRoman","fontBBox":[-170,-228,1003,962],"underlinePosition":-100,"underlineThickness":50,"capHeight":718,"xHeight":532,"ascender":718,"descender":-207,"kerning":{"44":{"146":-120,"148":-120,"160":-40},"46":{"146":-120,"148":-120,"160":-40},"58":{"160":-40},"59":{"160":-40},"65":{"67":-40,"71":-50,"79":-40,"81":-40,"84":-90,"85":-50,"86":-80,"87":-60,"89":-110,"117":-30,"118":-40,"119":-30,"121":-30,"199":-40,"210":-40,"211":-40,"212":-40,"213":-40,"214":-40,"216":-40,"217":-50,"218":-50,"219":-50,"220":-50,"221":-110,"249":-30,"250":-30,"251":-30,"252":-30,"253":-30,"255":-30},"66":{"65":-30,"85":-10,"192":-30,"193":-30,"194":-30,"195":-30,"196":-30,"197":-30,"217":-10,"218":-10,"219":-10,"220":-10},"68":{"44":-30,"46":-30,"65":-40,"86":-40,"87":-40,"89":-70,"192":-40,"193":-40,"194":-40,"195":-40,"196":-40,"197":-40,"221":-70},"70":{"44":-100,"46":-100,"65":-80,"97":-20,"192":-80,"193":-80,"194":-80,"195":-80,"196":-80,"197":-80,"224":-20,"225":-20,"226":-20,"227":-20,"228":-20,"229":-20},"74":{"44":-20,"46":-20,"65":-20,"117":-20,"192":-20,"193":-20,"194":-20,"195":-20,"196":-20,"197":-20,"249":-20,"250":-20,"251":-20,"252":-20},"75":{"79":-30,"101":-15,"111":-35,"117":-30,"121":-40,"210":-30,"211":-30,"212":-30,"213":-30,"214":-30,"216":-30,"232":-15,"233":-15,"234":-15,"235":-15,"242":-35,"243":-35,"244":-35,"245":-35,"246":-35,"248":-35,"249":-30,"250":-30,"251":-30,"252":-30,"253":-40,"255":-40},"76":{"84":-90,"86":-110,"87":-80,"89":-120,"121":-30,"146":-140,"148":-140,"221":-120,"253":-30,"255":-30},"79":{"44":-40,"46":-40,"65":-50,"84":-40,"86":-50,"87":-50,"88":-50,"89":-70,"192":-50,"193":-50,"194":-50,"195":-50,"196":-50,"197":-50,"221":-70},"80":{"44":-120,"46":-120,"65":-100,"97":-30,"101":-30,"111":-40,"192":-100,"193":-100,"194":-100,"195":-100,"196":-100,"197":-100,"224":-30,"225":-30,"226":-30,"227":-30,"228":-30,"229":-30,"232":-30,"233":-30,"234":-30,"235":-30,"242":-40,"243":-40,"244":-40,"245":-40,"246":-40,"248":-40},"81":{"44":20,"46":20,"85":-10,"217":-10,"218":-10,"219":-10,"220":-10},"82":{"79":-20,"84":-20,"85":-20,"86":-50,"87":-40,"89":-50,"210":-20,"211":-20,"212":-20,"213":-20,"214":-20,"216":-20,"217":-20,"218":-20,"219":-20,"220":-20,"221":-50},"84":{"44":-80,"46":-80,"58":-40,"59":-40,"65":-90,"79":-40,"97":-80,"101":-60,"111":-80,"114":-80,"117":-90,"119":-60,"121":-60,"173":-120,"192":-90,"193":-90,"194":-90,"195":-90,"196":-90,"197":-90,"210":-40,"211":-40,"212":-40,"213":-40,"214":-40,"216":-40,"224":-80,"225":-80,"226":-80,"227":-80,"228":-80,"229":-80,"232":-60,"233":-60,"234":-60,"235":-60,"242":-80,"243":-80,"244":-80,"245":-80,"246":-80,"248":-80,"249":-90,"250":-90,"251":-90,"252":-90,"253":-60,"255":-60},"85":{"44":-30,"46":-30,"65":-50,"192":-50,"193":-50,"194":-50,"195":-50,"196":-50,"197":-50},"86":{"44":-120,"46":-120,"58":-40,"59":-40,"65":-80,"71":-50,"79":-50,"97":-60,"101":-50,"111":-90,"117":-60,"173":-80,"192":-80,"193":-80,"194":-80,"195":-80,"196":-80,"197":-80,"210":-50,"211":-50,"212":-50,"213":-50,"214":-50,"216":-50,"224":-60,"225":-60,"226":-60,"227":-60,"228":-60,"229":-60,"232":-50,"233":-50,"234":-50,"235":-50,"242":-90,"243":-90,"244":-90,"245":-90,"246":-90,"248":-90,"249":-60,"250":-60,"251":-60,"252":-60},"87":{"44":-80,"46":-80,"58":-10,"59":-10,"65":-60,"79":-20,"97":-40,"101":-35,"111":-60,"117":-45,"121":-20,"173":-40,"192":-60,"193":-60,"194":-60,"195":-60,"196":-60,"197":-60,"210":-20,"211":-20,"212":-20,"213":-20,"214":-20,"216":-20,"224":-40,"225":-40,"226":-40,"227":-40,"228":-40,"229":-40,"232":-35,"233":-35,"234":-35,"235":-35,"242":-60,"243":-60,"244":-60,"245":-60,"246":-60,"248":-60,"249":-45,"250":-45,"251":-45,"252":-45,"253":-20,"255":-20},"89":{"44":-100,"46":-100,"58":-50,"59":-50,"65":-110,"79":-70,"97":-90,"101":-80,"111":-100,"117":-100,"192":-110,"193":-110,"194":-110,"195":-110,"196":-110,"197":-110,"210":-70,"211":-70,"212":-70,"213":-70,"214":-70,"216":-70,"224":-90,"225":-90,"226":-90,"227":-90,"228":-90,"229":-90,"232":-80,"233":-80,"234":-80,"235":-80,"242":-100,"243":-100,"244":-100,"245":-100,"246":-100,"248":-100,"249":-100,"250":-100,"251":-100,"252":-100},"97":{"103":-10,"118":-15,"119":-15,"121":-20,"253":-20,"255":-20},"98":{"108":-10,"117":-20,"118":-20,"121":-20,"249":-20,"250":-20,"251":-20,"252":-20,"253":-20,"255":-20},"99":{"104":-10,"107":-20,"108":-20,"121":-10,"253":-10,"255":-10},"100":{"100":-10,"118":-15,"119":-15,"121":-15,"253":-15,"255":-15},"101":{"44":10,"46":20,"118":-15,"119":-15,"120":-15,"121":-15,"253":-15,"255":-15},"102":{"44":-10,"46":-10,"101":-10,"111":-20,"146":30,"148":30,"232":-10,"233":-10,"234":-10,"235":-10,"242":-20,"243":-20,"244":-20,"245":-20,"246":-20,"248":-20},"103":{"101":10,"103":-10,"232":10,"233":10,"234":10,"235":10},"104":{"121":-20,"253":-20,"255":-20},"107":{"111":-15,"242":-15,"243":-15,"244":-15,"245":-15,"246":-15,"248":-15},"108":{"119":-15,"121":-15,"253":-15,"255":-15},"109":{"117":-20,"121":-30,"249":-20,"250":-20,"251":-20,"252":-20,"253":-30,"255":-30},"110":{"117":-10,"118":-40,"121":-20,"249":-10,"250":-10,"251":-10,"252":-10,"253":-20,"255":-20},"111":{"118":-20,"119":-15,"120":-30,"121":-20,"253":-20,"255":-20},"112":{"121":-15,"253":-15,"255":-15},"114":{"44":-60,"46":-60,"99":-20,"100":-20,"103":-15,"111":-20,"113":-20,"115":-15,"116":20,"118":10,"121":10,"154":-15,"173":-20,"231":-20,"242":-20,"243":-20,"244":-20,"245":-20,"246":-20,"248":-20,"253":10,"255":10},"115":{"119":-15},"118":{"44":-80,"46":-80,"97":-20,"111":-30,"224":-20,"225":-20,"226":-20,"227":-20,"228":-20,"229":-20,"242":-30,"243":-30,"244":-30,"245":-30,"246":-30,"248":-30},"119":{"44":-40,"46":-40,"111":-20,"242":-20,"243":-20,"244":-20,"245":-20,"246":-20,"248":-20},"120":{"101":-10,"232":-10,"233":-10,"234":-10,"235":-10},"121":{"44":-80,"46":-80,"97":-30,"101":-10,"111":-25,"224":-30,"225":-30,"226":-30,"227":-30,"228":-30,"229":-30,"232":-10,"233":-10,"234":-10,"235":-10,"242":-25,"243":-25,"244":-25,"245":-25,"246":-25,"248":-25},"122":{"101":10,"232":10,"233":10,"234":10,"235":10},"145":{"145":-46},"146":{"100":-80,"108":-20,"114":-40,"115":-60,"118":-20,"146":-46,"154":-60,"160":-80},"148":{"160":-80},"154":{"119":-15},"158":{"101":10,"232":10,"233":10,"234":10,"235":10},"160":{"84":-100,"86":-80,"87":-80,"89":-120,"145":-60,"147":-80,"221":-120},"192":{"67":-40,"71":-50,"79":-40,"81":-40,"84":-90,"85":-50,"86":-80,"87":-60,"89":-110,"117":-30,"118":-40,"119":-30,"121":-30,"199":-40,"210":-40,"211":-40,"212":-40,"213":-40,"214":-40,"216":-40,"217":-50,"218":-50,"219":-50,"220":-50,"221":-110,"249":-30,"250":-30,"251":-30,"252":-30,"253":-30,"255":-30},"193":{"67":-40,"71":-50,"79":-40,"81":-40,"84":-90,"85":-50,"86":-80,"87":-60,"89":-110,"117":-30,"118":-40,"119":-30,"121":-30,"199":-40,"210":-40,"211":-40,"212":-40,"213":-40,"214":-40,"216":-40,"217":-50,"218":-50,"219":-50,"220":-50,"221":-110,"249":-30,"250":-30,"251":-30,"252":-30,"253":-30,"255":-30},"194":{"67":-40,"71":-50,"79":-40,"81":-40,"84":-90,"85":-50,"86":-80,"87":-60,"89":-110,"117":-30,"118":-40,"119":-30,"121":-30,"199":-40,"210":-40,"211":-40,"212":-40,"213":-40,"214":-40,"216":-40,"217":-50,"218":-50,"219":-50,"220":-50,"221":-110,"249":-30,"250":-30,"251":-30,"252":-30,"253":-30,"255":-30},"195":{"67":-40,"71":-50,"79":-40,"81":-40,"84":-90,"85":-50,"86":-80,"87":-60,"89":-110,"117":-30,"118":-40,"119":-30,"121":-30,"199":-40,"210":-40,"211":-40,"212":-40,"213":-40,"214":-40,"216":-40,"217":-50,"218":-50,"219":-50,"220":-50,"221":-110,"249":-30,"250":-30,"251":-30,"252":-30,"253":-30,"255":-30},"196":{"67":-40,"71":-50,"79":-40,"81":-40,"84":-90,"85":-50,"86":-80,"87":-60,"89":-110,"117":-30,"118":-40,"119":-30,"121":-30,"199":-40,"210":-40,"211":-40,"212":-40,"213":-40,"214":-40,"216":-40,"217":-50,"218":-50,"219":-50,"220":-50,"221":-110,"249":-30,"250":-30,"251":-30,"252":-30,"253":-30,"255":-30},"197":{"67":-40,"71":-50,"79":-40,"81":-40,"84":-90,"85":-50,"86":-80,"87":-60,"89":-110,"117":-30,"118":-40,"119":-30,"121":-30,"199":-40,"210":-40,"211":-40,"212":-40,"213":-40,"214":-40,"216":-40,"217":-50,"218":-50,"219":-50,"220":-50,"221":-110,"249":-30,"250":-30,"251":-30,"252":-30,"253":-30,"255":-30},"210":{"44":-40,"46":-40,"65":-50,"84":-40,"86":-50,"87":-50,"88":-50,"89":-70,"192":-50,"193":-50,"194":-50,"195":-50,"196":-50,"197":-50,"221":-70},"211":{"44":-40,"46":-40,"65":-50,"84":-40,"86":-50,"87":-50,"88":-50,"89":-70,"192":-50,"193":-50,"194":-50,"195":-50,"196":-50,"197":-50,"221":-70},"212":{"44":-40,"46":-40,"65":-50,"84":-40,"86":-50,"87":-50,"88":-50,"89":-70,"192":-50,"193":-50,"194":-50,"195":-50,"196":-50,"197":-50,"221":-70},"213":{"44":-40,"46":-40,"65":-50,"84":-40,"86":-50,"87":-50,"88":-50,"89":-70,"192":-50,"193":-50,"194":-50,"195":-50,"196":-50,"197":-50,"221":-70},"214":{"44":-40,"46":-40,"65":-50,"84":-40,"86":-50,"87":-50,"88":-50,"89":-70,"192":-50,"193":-50,"194":-50,"195":-50,"196":-50,"197":-50,"221":-70},"216":{"44":-40,"46":-40,"65":-50,"84":-40,"86":-50,"87":-50,"88":-50,"89":-70,"192":-50,"193":-50,"194":-50,"195":-50,"196":-50,"197":-50,"221":-70},"217":{"44":-30,"46":-30,"65":-50,"192":-50,"193":-50,"194":-50,"195":-50,"196":-50,"197":-50},"218":{"44":-30,"46":-30,"65":-50,"192":-50,"193":-50,"194":-50,"195":-50,"196":-50,"197":-50},"219":{"44":-30,"46":-30,"65":-50,"192":-50,"193":-50,"194":-50,"195":-50,"196":-50,"197":-50},"220":{"44":-30,"46":-30,"65":-50,"192":-50,"193":-50,"194":-50,"195":-50,"196":-50,"197":-50},"221":{"44":-100,"46":-100,"58":-50,"59":-50,"65":-110,"79":-70,"97":-90,"101":-80,"111":-100,"117":-100,"192":-110,"193":-110,"194":-110,"195":-110,"196":-110,"197":-110,"210":-70,"211":-70,"212":-70,"213":-70,"214":-70,"216":-70,"224":-90,"225":-90,"226":-90,"227":-90,"228":-90,"229":-90,"232":-80,"233":-80,"234":-80,"235":-80,"242":-100,"243":-100,"244":-100,"245":-100,"246":-100,"248":-100,"249":-100,"250":-100,"251":-100,"252":-100},"224":{"103":-10,"118":-15,"119":-15,"121":-20,"253":-20,"255":-20},"225":{"103":-10,"118":-15,"119":-15,"121":-20,"253":-20,"255":-20},"226":{"103":-10,"118":-15,"119":-15,"121":-20,"253":-20,"255":-20},"227":{"103":-10,"118":-15,"119":-15,"121":-20,"253":-20,"255":-20},"228":{"103":-10,"118":-15,"119":-15,"121":-20,"253":-20,"255":-20},"229":{"103":-10,"118":-15,"119":-15,"121":-20,"253":-20,"255":-20},"231":{"104":-10,"107":-20,"108":-20,"121":-10,"253":-10,"255":-10},"232":{"44":10,"46":20,"118":-15,"119":-15,"120":-15,"121":-15,"253":-15,"255":-15},"233":{"44":10,"46":20,"118":-15,"119":-15,"120":-15,"121":-15,"253":-15,"255":-15},"234":{"44":10,"46":20,"118":-15,"119":-15,"120":-15,"121":-15,"253":-15,"255":-15},"235":{"44":10,"46":20,"118":-15,"119":-15,"120":-15,"121":-15,"253":-15,"255":-15},"241":{"117":-10,"118":-40,"121":-20,"249":-10,"250":-10,"251":-10,"252":-10,"253":-20,"255":-20},"242":{"118":-20,"119":-15,"120":-30,"121":-20,"253":-20,"255":-20},"243":{"118":-20,"119":-15,"120":-30,"121":-20,"253":-20,"255":-20},"244":{"118":-20,"119":-15,"120":-30,"121":-20,"253":-20,"255":-20},"245":{"118":-20,"119":-15,"120":-30,"121":-20,"253":-20,"255":-20},"246":{"118":-20,"119":-15,"120":-30,"121":-20,"253":-20,"255":-20},"248":{"118":-20,"119":-15,"120":-30,"121":-20,"253":-20,"255":-20},"253":{"44":-80,"46":-80,"97":-30,"101":-10,"111":-25,"224":-30,"225":-30,"226":-30,"227":-30,"228":-30,"229":-30,"232":-10,"233":-10,"234":-10,"235":-10,"242":-25,"243":-25,"244":-25,"245":-25,"246":-25,"248":-25},"255":{"44":-80,"46":-80,"97":-30,"101":-10,"111":-25,"224":-30,"225":-30,"226":-30,"227":-30,"228":-30,"229":-30,"232":-10,"233":-10,"234":-10,"235":-10,"242":-25,"243":-25,"244":-25,"245":-25,"246":-25,"248":-25}},"widths":[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,278,333,474,556,556,889,722,238,333,333,389,584,278,333,278,278,556,556,556,556,556,556,556,556,556,556,333,333,584,584,584,611,975,722,722,722,722,667,611,778,722,278,556,722,611,833,722,778,667,778,722,667,611,722,667,944,667,667,611,333,278,333,584,556,333,556,611,556,611,556,333,611,611,278,278,556,278,889,611,611,611,611,389,556,333,611,556,778,556,556,500,389,280,389,584,null,556,null,278,556,500,1000,556,556,333,1000,667,333,1000,null,611,null,null,278,278,500,500,350,556,1000,333,1000,556,333,944,null,500,556,278,333,556,556,556,556,280,556,333,737,370,556,584,333,737,333,400,584,333,333,333,611,556,278,333,333,365,556,834,834,834,611,722,722,722,722,722,722,1000,722,667,667,667,667,278,278,278,278,722,722,778,778,778,778,778,584,778,722,722,722,722,667,667,611,556,556,556,556,556,556,889,556,556,556,556,556,278,278,278,278,611,611,611,611,611,611,611,584,611,611,611,611,611,556,611,556]},

    'CourierBoldOblique': {"fontName":"Courier-BoldOblique","fullName":"Courier Bold Oblique","familyName":"Courier","italicAngle":-12,"characterSet":"ExtendedRoman","fontBBox":[-57,-250,869,801],"underlinePosition":-100,"underlineThickness":50,"capHeight":562,"xHeight":439,"ascender":629,"descender":-157,"kerning":{},"widths":[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,null,600,null,600,600,600,600,600,600,600,600,600,600,600,null,600,null,null,600,600,600,600,600,600,600,600,600,600,600,600,null,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600,600]},

    'Helvetica': {"fontName":"Helvetica","fullName":"Helvetica","familyName":"Helvetica","italicAngle":0,"characterSet":"ExtendedRoman","fontBBox":[-166,-225,1000,931],"underlinePosition":-100,"underlineThickness":50,"capHeight":718,"xHeight":523,"ascender":718,"descender":-207,"kerning":{"44":{"146":-100,"148":-100},"46":{"146":-100,"148":-100,"160":-60},"58":{"160":-50},"59":{"160":-50},"65":{"67":-30,"71":-30,"79":-30,"81":-30,"84":-120,"85":-50,"86":-70,"87":-50,"89":-100,"117":-30,"118":-40,"119":-40,"121":-40,"199":-30,"210":-30,"211":-30,"212":-30,"213":-30,"214":-30,"216":-30,"217":-50,"218":-50,"219":-50,"220":-50,"221":-100,"249":-30,"250":-30,"251":-30,"252":-30,"253":-40,"255":-40},"66":{"44":-20,"46":-20,"85":-10,"217":-10,"218":-10,"219":-10,"220":-10},"67":{"44":-30,"46":-30},"68":{"44":-70,"46":-70,"65":-40,"86":-70,"87":-40,"89":-90,"192":-40,"193":-40,"194":-40,"195":-40,"196":-40,"197":-40,"221":-90},"70":{"44":-150,"46":-150,"65":-80,"97":-50,"101":-30,"111":-30,"114":-45,"192":-80,"193":-80,"194":-80,"195":-80,"196":-80,"197":-80,"224":-50,"225":-50,"226":-50,"227":-50,"228":-50,"229":-50,"232":-30,"233":-30,"234":-30,"235":-30,"242":-30,"243":-30,"244":-30,"245":-30,"246":-30,"248":-30},"74":{"44":-30,"46":-30,"65":-20,"97":-20,"117":-20,"192":-20,"193":-20,"194":-20,"195":-20,"196":-20,"197":-20,"224":-20,"225":-20,"226":-20,"227":-20,"228":-20,"229":-20,"249":-20,"250":-20,"251":-20,"252":-20},"75":{"79":-50,"101":-40,"111":-40,"117":-30,"121":-50,"210":-50,"211":-50,"212":-50,"213":-50,"214":-50,"216":-50,"232":-40,"233":-40,"234":-40,"235":-40,"242":-40,"243":-40,"244":-40,"245":-40,"246":-40,"248":-40,"249":-30,"250":-30,"251":-30,"252":-30,"253":-50,"255":-50},"76":{"84":-110,"86":-110,"87":-70,"89":-140,"121":-30,"146":-160,"148":-140,"221":-140,"253":-30,"255":-30},"79":{"44":-40,"46":-40,"65":-20,"84":-40,"86":-50,"87":-30,"88":-60,"89":-70,"192":-20,"193":-20,"194":-20,"195":-20,"196":-20,"197":-20,"221":-70},"80":{"44":-180,"46":-180,"65":-120,"97":-40,"101":-50,"111":-50,"192":-120,"193":-120,"194":-120,"195":-120,"196":-120,"197":-120,"224":-40,"225":-40,"226":-40,"227":-40,"228":-40,"229":-40,"232":-50,"233":-50,"234":-50,"235":-50,"242":-50,"243":-50,"244":-50,"245":-50,"246":-50,"248":-50},"81":{"85":-10,"217":-10,"218":-10,"219":-10,"220":-10},"82":{"79":-20,"84":-30,"85":-40,"86":-50,"87":-30,"89":-50,"210":-20,"211":-20,"212":-20,"213":-20,"214":-20,"216":-20,"217":-40,"218":-40,"219":-40,"220":-40,"221":-50},"83":{"44":-20,"46":-20},"84":{"44":-120,"46":-120,"58":-20,"59":-20,"65":-120,"79":-40,"97":-120,"101":-120,"111":-120,"114":-120,"117":-120,"119":-120,"121":-120,"173":-140,"192":-120,"193":-120,"194":-120,"195":-120,"196":-120,"197":-120,"210":-40,"211":-40,"212":-40,"213":-40,"214":-40,"216":-40,"224":-120,"225":-120,"226":-120,"227":-60,"228":-120,"229":-120,"232":-60,"233":-120,"234":-120,"235":-120,"242":-120,"243":-120,"244":-120,"245":-60,"246":-120,"248":-120,"249":-120,"250":-120,"251":-120,"252":-120,"253":-120,"255":-60},"85":{"44":-40,"46":-40,"65":-40,"192":-40,"193":-40,"194":-40,"195":-40,"196":-40,"197":-40},"86":{"44":-125,"46":-125,"58":-40,"59":-40,"65":-80,"71":-40,"79":-40,"97":-70,"101":-80,"111":-80,"117":-70,"173":-80,"192":-80,"193":-80,"194":-80,"195":-80,"196":-80,"197":-80,"210":-40,"211":-40,"212":-40,"213":-40,"214":-40,"216":-40,"224":-70,"225":-70,"226":-70,"227":-70,"228":-70,"229":-70,"232":-80,"233":-80,"234":-80,"235":-80,"242":-80,"243":-80,"244":-80,"245":-80,"246":-80,"248":-80,"249":-70,"250":-70,"251":-70,"252":-70},"87":{"44":-80,"46":-80,"65":-50,"79":-20,"97":-40,"101":-30,"111":-30,"117":-30,"121":-20,"173":-40,"192":-50,"193":-50,"194":-50,"195":-50,"196":-50,"197":-50,"210":-20,"211":-20,"212":-20,"213":-20,"214":-20,"216":-20,"224":-40,"225":-40,"226":-40,"227":-40,"228":-40,"229":-40,"232":-30,"233":-30,"234":-30,"235":-30,"242":-30,"243":-30,"244":-30,"245":-30,"246":-30,"248":-30,"249":-30,"250":-30,"251":-30,"252":-30,"253":-20,"255":-20},"89":{"44":-140,"46":-140,"58":-60,"59":-60,"65":-110,"79":-85,"97":-140,"101":-140,"105":-20,"111":-140,"117":-110,"173":-140,"192":-110,"193":-110,"194":-110,"195":-110,"196":-110,"197":-110,"210":-85,"211":-85,"212":-85,"213":-85,"214":-85,"216":-85,"224":-140,"225":-140,"226":-140,"227":-140,"228":-140,"229":-140,"232":-140,"233":-140,"234":-140,"235":-140,"237":-20,"242":-140,"243":-140,"244":-140,"245":-140,"246":-140,"248":-140,"249":-110,"250":-110,"251":-110,"252":-110},"97":{"118":-20,"119":-20,"121":-30,"253":-30,"255":-30},"98":{"44":-40,"46":-40,"98":-10,"108":-20,"117":-20,"118":-20,"121":-20,"249":-20,"250":-20,"251":-20,"252":-20,"253":-20,"255":-20},"99":{"44":-15,"107":-20},"101":{"44":-15,"46":-15,"118":-30,"119":-20,"120":-30,"121":-20,"253":-20,"255":-20},"102":{"44":-30,"46":-30,"97":-30,"101":-30,"111":-30,"146":50,"148":60,"224":-30,"225":-30,"226":-30,"227":-30,"228":-30,"229":-30,"232":-30,"233":-30,"234":-30,"235":-30,"242":-30,"243":-30,"244":-30,"245":-30,"246":-30,"248":-30},"103":{"114":-10},"104":{"121":-30,"253":-30,"255":-30},"107":{"101":-20,"111":-20,"232":-20,"233":-20,"234":-20,"235":-20,"242":-20,"243":-20,"244":-20,"245":-20,"246":-20,"248":-20},"109":{"117":-10,"121":-15,"249":-10,"250":-10,"251":-10,"252":-10,"253":-15,"255":-15},"110":{"117":-10,"118":-20,"121":-15,"249":-10,"250":-10,"251":-10,"252":-10,"253":-15,"255":-15},"111":{"44":-40,"46":-40,"118":-15,"119":-15,"120":-30,"121":-30,"253":-30,"255":-30},"112":{"44":-35,"46":-35,"121":-30,"253":-30,"255":-30},"114":{"44":-50,"46":-50,"58":30,"59":30,"97":-10,"105":15,"107":15,"108":15,"109":25,"110":25,"112":30,"116":40,"117":15,"118":30,"121":30,"224":-10,"225":-10,"226":-10,"227":-10,"228":-10,"229":-10,"236":15,"237":15,"238":15,"239":15,"241":25,"249":15,"250":15,"251":15,"252":15,"253":30,"255":30},"115":{"44":-15,"46":-15,"119":-30},"118":{"44":-80,"46":-80,"97":-25,"101":-25,"111":-25,"224":-25,"225":-25,"226":-25,"227":-25,"228":-25,"229":-25,"232":-25,"233":-25,"234":-25,"235":-25,"242":-25,"243":-25,"244":-25,"245":-25,"246":-25,"248":-25},"119":{"44":-60,"46":-60,"97":-15,"101":-10,"111":-10,"224":-15,"225":-15,"226":-15,"227":-15,"228":-15,"229":-15,"232":-10,"233":-10,"234":-10,"235":-10,"242":-10,"243":-10,"244":-10,"245":-10,"246":-10,"248":-10},"120":{"101":-30,"232":-30,"233":-30,"234":-30,"235":-30},"121":{"44":-100,"46":-100,"97":-20,"101":-20,"111":-20,"224":-20,"225":-20,"226":-20,"227":-20,"228":-20,"229":-20,"232":-20,"233":-20,"234":-20,"235":-20,"242":-20,"243":-20,"244":-20,"245":-20,"246":-20,"248":-20},"122":{"101":-15,"111":-15,"232":-15,"233":-15,"234":-15,"235":-15,"242":-15,"243":-15,"244":-15,"245":-15,"246":-15,"248":-15},"138":{"44":-20,"46":-20},"145":{"145":-57},"146":{"100":-50,"114":-50,"115":-50,"146":-57,"154":-50,"160":-70},"148":{"160":-40},"154":{"44":-15,"46":-15,"119":-30},"158":{"101":-15,"111":-15,"232":-15,"233":-15,"234":-15,"235":-15,"242":-15,"243":-15,"244":-15,"245":-15,"246":-15,"248":-15},"160":{"84":-50,"86":-50,"87":-40,"89":-90,"145":-60,"147":-30,"221":-90},"192":{"67":-30,"71":-30,"79":-30,"81":-30,"84":-120,"85":-50,"86":-70,"87":-50,"89":-100,"117":-30,"118":-40,"119":-40,"121":-40,"199":-30,"210":-30,"211":-30,"212":-30,"213":-30,"214":-30,"216":-30,"217":-50,"218":-50,"219":-50,"220":-50,"221":-100,"249":-30,"250":-30,"251":-30,"252":-30,"253":-40,"255":-40},"193":{"67":-30,"71":-30,"79":-30,"81":-30,"84":-120,"85":-50,"86":-70,"87":-50,"89":-100,"117":-30,"118":-40,"119":-40,"121":-40,"199":-30,"210":-30,"211":-30,"212":-30,"213":-30,"214":-30,"216":-30,"217":-50,"218":-50,"219":-50,"220":-50,"221":-100,"249":-30,"250":-30,"251":-30,"252":-30,"253":-40,"255":-40},"194":{"67":-30,"71":-30,"79":-30,"81":-30,"84":-120,"85":-50,"86":-70,"87":-50,"89":-100,"117":-30,"118":-40,"119":-40,"121":-40,"199":-30,"210":-30,"211":-30,"212":-30,"213":-30,"214":-30,"216":-30,"217":-50,"218":-50,"219":-50,"220":-50,"221":-100,"249":-30,"250":-30,"251":-30,"252":-30,"253":-40,"255":-40},"195":{"67":-30,"71":-30,"79":-30,"81":-30,"84":-120,"85":-50,"86":-70,"87":-50,"89":-100,"117":-30,"118":-40,"119":-40,"121":-40,"199":-30,"210":-30,"211":-30,"212":-30,"213":-30,"214":-30,"216":-30,"217":-50,"218":-50,"219":-50,"220":-50,"221":-100,"249":-30,"250":-30,"251":-30,"252":-30,"253":-40,"255":-40},"196":{"67":-30,"71":-30,"79":-30,"81":-30,"84":-120,"85":-50,"86":-70,"87":-50,"89":-100,"117":-30,"118":-40,"119":-40,"121":-40,"199":-30,"210":-30,"211":-30,"212":-30,"213":-30,"214":-30,"216":-30,"217":-50,"218":-50,"219":-50,"220":-50,"221":-100,"249":-30,"250":-30,"251":-30,"252":-30,"253":-40,"255":-40},"197":{"67":-30,"71":-30,"79":-30,"81":-30,"84":-120,"85":-50,"86":-70,"87":-50,"89":-100,"117":-30,"118":-40,"119":-40,"121":-40,"199":-30,"210":-30,"211":-30,"212":-30,"213":-30,"214":-30,"216":-30,"217":-50,"218":-50,"219":-50,"220":-50,"221":-100,"249":-30,"250":-30,"251":-30,"252":-30,"253":-40,"255":-40},"199":{"44":-30,"46":-30},"210":{"44":-40,"46":-40,"65":-20,"84":-40,"86":-50,"87":-30,"88":-60,"89":-70,"192":-20,"193":-20,"194":-20,"195":-20,"196":-20,"197":-20,"221":-70},"211":{"44":-40,"46":-40,"65":-20,"84":-40,"86":-50,"87":-30,"88":-60,"89":-70,"192":-20,"193":-20,"194":-20,"195":-20,"196":-20,"197":-20,"221":-70},"212":{"44":-40,"46":-40,"65":-20,"84":-40,"86":-50,"87":-30,"88":-60,"89":-70,"192":-20,"193":-20,"194":-20,"195":-20,"196":-20,"197":-20,"221":-70},"213":{"44":-40,"46":-40,"65":-20,"84":-40,"86":-50,"87":-30,"88":-60,"89":-70,"192":-20,"193":-20,"194":-20,"195":-20,"196":-20,"197":-20,"221":-70},"214":{"44":-40,"46":-40,"65":-20,"84":-40,"86":-50,"87":-30,"88":-60,"89":-70,"192":-20,"193":-20,"194":-20,"195":-20,"196":-20,"197":-20,"221":-70},"216":{"44":-40,"46":-40,"65":-20,"84":-40,"86":-50,"87":-30,"88":-60,"89":-70,"192":-20,"193":-20,"194":-20,"195":-20,"196":-20,"197":-20,"221":-70},"217":{"44":-40,"46":-40,"65":-40,"192":-40,"193":-40,"194":-40,"195":-40,"196":-40,"197":-40},"218":{"44":-40,"46":-40,"65":-40,"192":-40,"193":-40,"194":-40,"195":-40,"196":-40,"197":-40},"219":{"44":-40,"46":-40,"65":-40,"192":-40,"193":-40,"194":-40,"195":-40,"196":-40,"197":-40},"220":{"44":-40,"46":-40,"65":-40,"192":-40,"193":-40,"194":-40,"195":-40,"196":-40,"197":-40},"221":{"44":-140,"46":-140,"58":-60,"59":-60,"65":-110,"79":-85,"97":-140,"101":-140,"105":-20,"111":-140,"117":-110,"173":-140,"192":-110,"193":-110,"194":-110,"195":-110,"196":-110,"197":-110,"210":-85,"211":-85,"212":-85,"213":-85,"214":-85,"216":-85,"224":-140,"225":-140,"226":-140,"227":-70,"228":-140,"229":-140,"232":-140,"233":-140,"234":-140,"235":-140,"237":-20,"242":-140,"243":-140,"244":-140,"245":-140,"246":-140,"248":-140,"249":-110,"250":-110,"251":-110,"252":-110},"224":{"118":-20,"119":-20,"121":-30,"253":-30,"255":-30},"225":{"118":-20,"119":-20,"121":-30,"253":-30,"255":-30},"226":{"118":-20,"119":-20,"121":-30,"253":-30,"255":-30},"227":{"118":-20,"119":-20,"121":-30,"253":-30,"255":-30},"228":{"118":-20,"119":-20,"121":-30,"253":-30,"255":-30},"229":{"118":-20,"119":-20,"121":-30,"253":-30,"255":-30},"231":{"44":-15,"107":-20},"232":{"44":-15,"46":-15,"118":-30,"119":-20,"120":-30,"121":-20,"253":-20,"255":-20},"233":{"44":-15,"46":-15,"118":-30,"119":-20,"120":-30,"121":-20,"253":-20,"255":-20},"234":{"44":-15,"46":-15,"118":-30,"119":-20,"120":-30,"121":-20,"253":-20,"255":-20},"235":{"44":-15,"46":-15,"118":-30,"119":-20,"120":-30,"121":-20,"253":-20,"255":-20},"241":{"117":-10,"118":-20,"121":-15,"249":-10,"250":-10,"251":-10,"252":-10,"253":-15,"255":-15},"242":{"44":-40,"46":-40,"118":-15,"119":-15,"120":-30,"121":-30,"253":-30,"255":-30},"243":{"44":-40,"46":-40,"118":-15,"119":-15,"120":-30,"121":-30,"253":-30,"255":-30},"244":{"44":-40,"46":-40,"118":-15,"119":-15,"120":-30,"121":-30,"253":-30,"255":-30},"245":{"44":-40,"46":-40,"118":-15,"119":-15,"120":-30,"121":-30,"253":-30,"255":-30},"246":{"44":-40,"46":-40,"118":-15,"119":-15,"120":-30,"121":-30,"253":-30,"255":-30},"248":{"44":-95,"46":-95,"97":-55,"98":-55,"99":-55,"100":-55,"101":-55,"102":-55,"103":-55,"104":-55,"105":-55,"106":-55,"107":-55,"108":-55,"109":-55,"110":-55,"111":-55,"112":-55,"113":-55,"114":-55,"115":-55,"116":-55,"117":-55,"118":-70,"119":-70,"120":-85,"121":-70,"122":-55,"154":-55,"158":-55,"224":-55,"225":-55,"226":-55,"227":-55,"228":-55,"229":-55,"231":-55,"232":-55,"233":-55,"234":-55,"235":-55,"236":-55,"237":-55,"238":-55,"239":-55,"241":-55,"242":-55,"243":-55,"244":-55,"245":-55,"246":-55,"248":-55,"249":-55,"250":-55,"251":-55,"252":-55,"253":-70,"255":-70},"253":{"44":-100,"46":-100,"97":-20,"101":-20,"111":-20,"224":-20,"225":-20,"226":-20,"227":-20,"228":-20,"229":-20,"232":-20,"233":-20,"234":-20,"235":-20,"242":-20,"243":-20,"244":-20,"245":-20,"246":-20,"248":-20},"255":{"44":-100,"46":-100,"97":-20,"101":-20,"111":-20,"224":-20,"225":-20,"226":-20,"227":-20,"228":-20,"229":-20,"232":-20,"233":-20,"234":-20,"235":-20,"242":-20,"243":-20,"244":-20,"245":-20,"246":-20,"248":-20}},"widths":[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,278,278,355,556,556,889,667,191,333,333,389,584,278,333,278,278,556,556,556,556,556,556,556,556,556,556,278,278,584,584,584,556,1015,667,667,722,722,667,611,778,722,278,500,667,556,833,722,778,667,778,722,667,611,722,667,944,667,667,611,278,278,278,469,556,333,556,556,500,556,556,278,556,556,222,222,500,222,833,556,556,556,556,333,500,278,556,500,722,500,500,500,334,260,334,584,null,556,null,222,556,333,1000,556,556,333,1000,667,333,1000,null,611,null,null,222,222,333,333,350,556,1000,333,1000,500,333,944,null,500,500,278,333,556,556,556,556,260,556,333,737,370,556,584,333,737,333,400,584,333,333,333,556,537,278,333,333,365,556,834,834,834,611,667,667,667,667,667,667,1000,722,667,667,667,667,278,278,278,278,722,722,778,778,778,778,778,584,778,722,722,722,722,667,667,611,556,556,556,556,556,556,889,500,556,556,556,556,278,278,278,278,556,556,556,556,556,556,556,584,611,556,556,556,556,500,556,500]}
};

let fonts = {
    HelveticaBold: new pdf.Font(fontData.HelveticaBold),
    Helvetica: new pdf.Font(fontData.Helvetica),
    // HelveticaOblique: new pdf.Font(require('pdfjs/font/Helvetica-Oblique.json')),
    // Helvetica: new pdf.Font(require('pdfjs/font/helvetica.json')),
    // CourierBoldOblique: new pdf.Font(require('pdfjs/font/Courier-BoldOblique.json'))
    CourierBoldOblique: new pdf.Font(fontData.CourierBoldOblique)
};

async function sendEmail(pdfFileName, name, destination, subject, text) {
    var CRLF = '\r\n';
    //let buf = fs.readFileSync(pdfFileName);

    fs.readFile(pdfFileName, (err, buf) => {
        if (err) {
            console.log("Error ", err);
            return;
        }

        let unixTime = Math.floor(new Date() / 1000);                
     
        let encBase64File = buf.toString('base64');
        
        let rMsg = [
            'From: "MyPromi.se" <contact@mypromi.se>',
            'To: "' + name +'" <' + destination + '>',
            'Subject: ' + subject,
            'Content-Type: multipart/mixed;boundary="_003_97DCB304C5294779BEBCFC8357FCC4D2"',
            'MIME-Version: 1.0',
            '',
            '--_003_97DCB304C5294779BEBCFC8357FCC4D2',
            'Content-Type: text/plain; charset="us-ascii"',
            'Content-Transfer-Encoding: quoted-printable',
            '',
            'Hi ' + name + ',',
            '',
            'Here is your gift certificate for fulfilling your promise',
            'Good job !',
            '',
            'Cheers.',
            'MyPromi.se Team',
            '',
            '--_003_97DCB304C5294779BEBCFC8357FCC4D2',
            'Content-Type: application/pdf; name="CertificateOfAchievement.pdf"',
            'Content-Description: CertificateOfAchievement.pdf',
            'Content-Disposition: attachment; filename="CertificateOfAchievement.pdf"',   
            'Content-Transfer-Encoding: base64',
            '',
            encBase64File,
            '--_003_97DCB304C5294779BEBCFC8357FCC4D2--',
            ''
            ].join(CRLF);
    
            if (_sendEmail === false) {
                fs.writeFileSync("/tmp/plm1.pdf", Buffer.from(encBase64File, 'base64'));
                fs.writeFileSync("/tmp/plm1.txt", encBase64File);          
            }
            console.log("PDF NAME: ", pdfFileName);
            
    
            var params = {
            RawMessage: { /* required */
                Data: rMsg
            },
            Destinations: [destination]
            }
        
        if (_sendEmail) {
            const AWS = require('aws-sdk')        
            var ses = new AWS.SES();        
            ses.sendRawEmail(params, function(err, data) {
                if (err) { 
                    console.log(EVT_SOURCE_ERR, err, err.stack);
                } // an error occurred
                else { 
                    console.log(EVT_SOURCE_OK, data); 
                    console.log("Sent mail");
                }
            });  
        }    
    });
}

async function genPdf(email, name, promiseName, deadlineDays, fileBuffer, cb) {
   //A4 format... 841.896,  595.296, 1.41424 ratio

  console.log("PARAMS genPdf");
  console.log(email, name, promiseName, deadlineDays);

  const doc = new pdf.Document({
      font:    fonts.Helvetica,
      padding: 10,
      width:  841.896,
      height: 595.296
  });

  let unixTime = Math.floor(new Date() / 1000);        
  pdfFileName = "/tmp/cert-" + unixTime + name + deadlineDays + ".pdf";        
  
  let wStream = fs.createWriteStream(pdfFileName);
  doc.pipe(wStream);
  // render something onto the document

    console.log("Dir name: ", __dirname);

    const pattern = new pdf.Image(fs.readFileSync(__dirname + '/pattern1.jpg'));
    const pattern2 = new pdf.Image(fs.readFileSync(__dirname + '/pattern2.jpg'));
    const pattern3 = new pdf.Image(fs.readFileSync(__dirname + '/pattern3.jpg'));
    const pattern4 = new pdf.Image(fs.readFileSync(__dirname + '/pattern4.jpg'));

    var header = doc.header().table({ widths: [null,null], paddingBottom: 0.2*pdf.cm }).row();
    header.cell().image(pattern, {
        align: 'left',
        height: 80
    });

    header.cell().image(pattern2, {
        align: 'right',
        height: 80
    });

    //const footer = doc.footer();
   // footer.pageNumber({ textAlign: 'center' })
    const footer = doc.footer().table({ widths: [null,null], paddingBottom: 0.1*pdf.cm }).row();
    footer.cell().image(pattern3, {
        align: 'left',
        height: 80
    });

    footer.cell().image(pattern4, {
        align: 'right',
        height: 80
    });
  //header.cell().text({textAlign: 'left'}).add('MyPromi.se', { font: fonts.HelveticaOblique, fontSize: 6});
 // header.cell().text({ textAlign: 'right' }).add('');
   
 var cell = doc.cell({ paddingBottom: 0.5*pdf.cm });
 cell.text('Certificate of Achievement', 
            { 
              fontSize: 34, 
              font: fonts.CourierBoldOblique, 
              textAlign: 'center' 
            });

  //Intro cell
  doc.cell({ paddingBottom: 0.3 * pdf.cm })
      .text('This is to certify that', { textAlign: 'center' })
      .add(name, { font: fonts.HelveticaBold, textAlign: 'center' })
      .add('has fulfilled the promise', { textAlign: 'center' });

  //promise cell
  doc.cell({paddingBottom: 0.3 * pdf.cm })
      .text(promiseName, { font: fonts.HelveticaBold, fontSize: 26, textAlign: 'center'});

  doc.cell({paddingBottom: 0.5 * pdf.cm})    
      .text(' in ' + deadlineDays + ' days from the original commitment ', { textAlign: 'center' });        

  console.log("Directory: ", __dirname);
    
  const imgFile = new pdf.Image(fileBuffer, {height: 2*pdf.cm});
  
  doc.cell({paddingBottom: 0.1*pdf.cm}).image(imgFile, {align: 'center', height: 240}); 

  doc.cell({paddingTop: 0.2* pdf.cm})    
  .text(' Issued by www.mypromi.se on ' + new Date().toString(), {fontSize: 6, textAlign: 'center' });        


  await doc.end(); 

  wStream.on('close', async () => {
    
      var responseCode = 200;
      var responseBody = { status: 'OK' };
    
      var response = {
          statusCode: responseCode,
          headers: {
              "access-control-allow-origin": "*"
          },
          body: JSON.stringify(responseBody)
      };
    
      await  sendEmail(pdfFileName, name ,email, 'MyPromi.se - a gift for keeping your promise', 'Here is your promise');
     cb(null, response);      
  })

}

module.exports = {genPdf};