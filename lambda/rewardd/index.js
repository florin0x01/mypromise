require('babel-polyfill');
var x = require('./src2');
const MyBucket = 'mypromise-storage0-ok'
const formatSeparator = '#';
const AWS = require('aws-sdk');

function makeHTTPError(cb, err, code=401) {
    let responseCode=code;
    let responseBody = { status: err };
    var response = {
        statusCode: responseCode,
        headers: {
           // "access-control-allow-origin": "*"
        },
        body: JSON.stringify(responseBody)
    };
    cb(null, response);
}

function makeHTTPSuccess(cb, code=200) {
    let responseCode=code;
    let responseBody = { status: 'OK' };
    var response = {
        statusCode: responseCode,
        headers: {
            "access-control-allow-origin": "*"
        },
        body: JSON.stringify(responseBody)
    };

    cb(null, response);
}

exports.handler = function (request, context, callback) {
    console.log(JSON.stringify(request, null, 2));
    let record = request.Records[0];
    let objKey = unescape(record.s3.object.key);
    let parts = objKey.split("/");
    let daystoAccomplish = parseInt(parts[0].substring("ex".length),10);
    let contactsInfo = parts[2].split(formatSeparator);
    console.log("Contacts info: ", contactsInfo);
    let uName = contactsInfo[0];
    let email = contactsInfo[1];
    let friendMail = contactsInfo[2];
    let promiseName = Buffer.from(contactsInfo[3], 'base64').toString();

    var s3 = new AWS.S3();    

    console.log("Object key: ", objKey);
    console.log("EMAIL: ", email);
    console.log("UNAME: ", uName);
    console.log("Promise name: ", promiseName);
    console.log("Days to accomplish: ", daystoAccomplish);

    s3.getObject({
            Bucket: MyBucket,
            Key: objKey
        }, function(err, obj) {
            if (err || obj === null) {
                if (err === null) err = {stack: []};
                console.log(EVT_SOURCE_ERR, "Error getting object ", objKey, err.stack); // an error occurred
                makeHTTPError(callback, 'Error');
                return;
            }

            x.genPdf(email, uName, promiseName , daystoAccomplish, obj.Body, function(err, resp) {
                return callback(err, resp);        
            });
        }
    );
}





