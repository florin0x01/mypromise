const AWS = require('aws-sdk');
const aes256 = require('aes256');

const EVT_SOURCE_ERR = "[MYPROMISE_LAMBDA_PROCESS][ERR] ";
const EVT_SOURCE_OK = "[MYPROMISE_LAMBDA_PROCESS][OK] ";

const S3SrcBucket = "mypromise-storage0";
const S3FailedBucket = "mypromise-storage0-failed";
const S3OkBucket = "mypromise-storage0-ok";

//processes the promise 
// => when clicking on YES/NO link from email

function getEncryptionText(txt) {
    const key = process.env.ENCKEY;
    var now = new Date(); 
    var now_utc = new Date(now.getUTCFullYear(), now.getUTCMonth());    
    const finalKey = key + now_utc;
    console.log("Final key ENC: ", finalKey);    
    return aes256.encrypt(finalKey, txt);
}

function getDecryptionText(txt) {
    const key = process.env.ENCKEY;
    var now = new Date(); 
    var now_utc = new Date(now.getUTCFullYear(), now.getUTCMonth());
    const finalKey = key + now_utc;
    console.log("Final key DEC: ", finalKey);
    return aes256.decrypt(finalKey, txt);    
}

function makeHTTPError(cb, err, code=401) {
    let responseCode=code;
    let responseBody = { status: err };
    var response = {
        statusCode: responseCode,
        headers: {
            "access-control-allow-origin": "*"
        },
        body: JSON.stringify(responseBody)
    };
    cb(null, response);
}

function makeHTTPSuccess(cb, code=200) {
    let responseCode=code;
    let responseBody = { status: 'OK' };
    var response = {
        statusCode: responseCode,
        headers: {
            "access-control-allow-origin": "*"
        },
        body: 'Thank you. The information has been processed. You may now close the page.'
    };

    cb(null, response);
}

exports.handler = (request, context, callback) => {
    // TODO implement
    console.log("API Event", request);
    console.log("Headers: ", request);

    if (request.pathParameters['versionId'] === undefined) {
         makeHTTPError(callback, 'Error');
         return;
    }
    let pathComponent = decodeURIComponent(request.pathParameters['versionId']);
    pathComponent = getDecryptionText(pathComponent);
    
    //Decode path component
    console.log("Path component: ", pathComponent);

    let dstBucket = "";

    if (request.path.indexOf("/unfulfill") === 0) {
        dstBucket = S3FailedBucket;
    }
    else if (request.path.indexOf("/fulfill") === 0) {
        dstBucket = S3OkBucket;    
    }
    else {
        makeHTTPError(callback, 'Unauthorized');
        return;
    }
    //copy to failed/OK S3 bucket and delete from orig bucket
    let s3 = new AWS.S3();
    s3.listObjectVersions({
            Bucket: S3SrcBucket,
            Prefix: pathComponent,
        }, 
        function(err, versions) {
            console.log("Error: ", err);
            console.log("Versions: ", versions);
            if (err || versions.Versions.length === 0) {
                if (err === null) err = {stack: []};
                console.log(EVT_SOURCE_ERR, "Error listing object ", pathComponent, err.stack); // an error occurred
                makeHTTPError(callback, 'Error listing obj');
                return;
            }
            let version = versions.Versions[0].VersionId;
            console.log("Got version ", version, " for key ", pathComponent);
            
            s3.getObject({
                    Bucket: S3SrcBucket,
                    Key: pathComponent,
                    VersionId: version
                }, function(err, obj) {
                    if (err || obj === null) {
                        if (err === null) err = {stack: []};
                        console.log(EVT_SOURCE_ERR, "Error copying object ", pathComponent, err.stack); // an error occurred
                        makeHTTPError(callback, 'Error');
                        return;
                    }
                    // console.log("Copied object to bucket ", dstBucket," now deleting");
                    s3.putObject({
                            Body: obj.Body,
                            Bucket: dstBucket,
                            Key: pathComponent,
                            ServerSideEncryption: "AES256",
                            Metadata: obj.Metadata
                        }, function(err, putFinished) {

                            if (err) {
                                console.log(EVT_SOURCE_ERR, "Error putting object to dst bucket", dstBucket, err.stack); // an error occurred        
                                makeHTTPError(callback, 'Error');   
                                return;   
                            }

                            console.log("PUT object to dst bucket ", dstBucket, " finished ")

                            s3.deleteObject({
                                    Bucket: S3SrcBucket,
                                    Key: pathComponent,
                                    VersionId: version
                                }, function(err,deletedData) {
                                    if (err) {
                                        console.log(EVT_SOURCE_ERR, "Error deleting object", err.stack); // an error occurred        
                                        makeHTTPError(callback, 'Error');   
                                        return;                                     
                                    }                                
                                    
                                    console.log("Deleted object from source bucket ", deletedData);
                                    makeHTTPSuccess(callback);       
                                    return;                                    
                                    
                            });         
                        });
                });
        });
  }