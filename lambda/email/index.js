//myPromise-SendEmail

const principal = "A1I1I4UI6E5IP6";
const bucketName = "mypromise-storage0";
const formatSeparator = '#';

const principalId = "A1I1I4UI6E5IP6";
const S3BucketName = "mypromise-storage0";
const srcEmail = "contact@mypromi.se";

const EVT_SOURCE_ERR = "[MYPROMISE_LAMBDA_SENDEMAIL][ERR] ";
const EVT_SOURCE_OK = "[MYPROMISE_LAMBDA_SENDEMAIL][OK] ";
const sendEmail = true; 

const AWS = require('aws-sdk')
const aes256 = require('aes256');
const sprintf = require("sprintf-js").sprintf;

const _YesLink = "https://mckmetyuci.execute-api.us-east-2.amazonaws.com/dev/fulfill/";
const _NoLink = "https://mckmetyuci.execute-api.us-east-2.amazonaws.com/dev/unfulfill/";

function getEncryptionText(txt) {
    const key = process.env.ENCKEY;
    console.log("Key: ", key);
    var now = new Date(); 
    var now_utc = new Date(now.getUTCFullYear(), now.getUTCMonth());    
    const finalKey = key + now_utc;
    console.log("Final Key ENC: ", finalKey);    
    return aes256.encrypt(finalKey, txt);
}
    
function getDecryptionText(txt) {
    const key = process.env.ENCKEY;
    var now = new Date(); 
    var now_utc = new Date(now.getUTCFullYear(), now.getUTCMonth());
    const finalKey = key + now_utc;
    console.log("Final Key DEC: ", finalKey);    
    return aes256.decrypt(finalKey, txt);    
}

function parseNotification(record) {
    if (record.eventName !== "ObjectRemoved:DeleteMarkerCreated" || 
            record.userIdentity.principalId !== principalId) {
        console.log(EVT_SOURCE_ERR + "Invalid identity or event name for record ", 
            record.eventName, record.userIdentity.principalId);
        return; 
    }
    if (record.s3.bucket.name !== S3BucketName) {
        console.log(EVT_SOURCE_ERR + "Invalid S3 Bucket Name ", record.s3.bucket.name);   
        return;     
    }

    //key is in base64 format
    let key = unescape(record.s3.object.key);
    console.log("Key: ", key);
    let version = record.s3.object.versionId;
    let parts = key.split("/");

    let expiry = parts[1];
    let contactsInfo = parts[2].split(formatSeparator);
    let uName = contactsInfo[0];
    let email = contactsInfo[1];
    let friendMail = contactsInfo[2];
    let mypromise = Buffer.from(contactsInfo[3], 'base64').toString();

    console.log("contactsInfo: ", JSON.stringify(contactsInfo, null, 2));   
  
    var ses = new AWS.SES();
    var s3 = new AWS.S3();

    let  YesLink = _YesLink;
    let  NoLink = _NoLink;
  
    var params2 = {Bucket: 'mypromise-storage0', Prefix: key};
    let _pathParam = getEncryptionText(key);
    let pathParam = encodeURIComponent(_pathParam);

    console.log("Encrypted text: ", _pathParam);
    console.log("Encoded encrypted text: ", pathParam);

    YesLink =  YesLink + pathParam;
    NoLink  =  NoLink  + pathParam;

    //generate API GW links

    console.log("YesLink URL: ", YesLink);
    console.log("NoLink URL: ", NoLink);

    let HtmlData = "<html>\
        <head>\
        </head>\
        <body>\
            Hi, <br /> You have been selected to check on your friend's %s promise - \
            <b>%s </b> <br />So, was the promise fulfilled? <br />\
            <a id='YES' href='%s' name='YES' target='_blank' style='background-color: #1166CD;border:none;color:white;padding: 15px 32px;text-align: center;text-decoration: none;display: inline-block;font-size: 16px;margin: 4px 2px;cursor:pointer'>YES</a> \
            <a id='NO' href='%s' name='NO' target='_blank' style='background-color: #CD1166;border:none;color:white;padding: 15px 32px;text-align: center;text-decoration: none;display: inline-block;font-size: 16px;margin: 4px 2px;cursor:pointer'>NO</a> \
            <br /><br /> \
            Please wait a few seconds for us to process the information. \
            A new page will show up. Please don't close the new page until it says it's ready to be closed. \
            <br /><br /> \
            Best, <br /> Mypromi.se team \
            <a href='https://www.mypromi.se'>www.mypromi.se</a> \
        </body>\
        </html>";

    let TxtData = "Hi, \r\n You have been selected to check on your friend's %s promise - %s\r\n\
                    So, what's the status? \r\n\
                    Click YES here if the promise was fulfilled:  <a href='%s'>YES</a>\r\n\
                    Click NO here if the promise was not fulfilled: <a href='%s'>NO</a>\r\n\
                    Best, \r\n MyPromi.se team \r\n\
                    <a href='https://www.mypromi.se'>www.mypromi.se</a>"    

    var params = {
        Destination: {
            ToAddresses: [ friendMail ]
        }, 
        Message: {
            Body: {
                Html: {
                    Charset: "UTF-8", 
                    Data: sprintf(HtmlData, uName, mypromise, YesLink, NoLink)
                }, 
                Text: {
                    Charset: "UTF-8", 
                    Data: sprintf(TxtData, uName, mypromise, YesLink, NoLink)
                }
            }, 
            Subject: {
                Charset: "UTF-8", 
                Data: "[mypromi.se] Related to a friend's promise - " + mypromise
            }
        }, 
        Source: srcEmail
    }; 

    if (sendEmail) {
        ses.sendEmail(params, function(err, data) {
            if (err) { 
                console.log(EVT_SOURCE_ERR, err, err.stack);
            } // an error occurred
            else { 
                console.log(EVT_SOURCE_OK, data); 
            }
        });   
    }
}

exports.handler = function(event, context, callback) {        
    console.log('Received event:', JSON.stringify(event, null, 2));
    for (let i=0 ; i < event.Records.length; i++) {
        parseNotification(event.Records[i]);
    }
}